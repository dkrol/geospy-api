﻿using GeoSpy.DAL.DataManager;
using GeoSpy.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GeoSpy.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Locations")]
    [EnableCorsAttribute("http://localhost:3000", "*", "*")]
    public class LocationsController : ApiController
    {
        private LocationDataManager locationDataManager = new LocationDataManager();

        // GET: api/Locations
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(locationDataManager.GetLocations());
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/Locations/5
        public string Get(int id)
        {
            return "value";
        }

        // GET: api/Locations/Device?id=5a5a
        [Route("Device")]
        public IHttpActionResult Get(string id)
        {
            try
            {
                return Ok(locationDataManager.GetLocationByDeviceId(id));
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Locations/Device
        [Route("Device")]
        public IHttpActionResult Post([FromBody]Device device)
        {
            try
            {
                return Ok(locationDataManager.GetLocationsByDevice(device));
            } 
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Locations/Device?id=222
        [Route("Device")]
        public IHttpActionResult Post(string id, [FromBody]DatetimeRange datetimeRange)
        {
            try
            {
                return Ok(locationDataManager.GetLocationsByDeviceAndDatetimeRange(id, datetimeRange));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Locations
        public void Post([FromBody]Location location)
        {
            bool result = locationDataManager.AddLocation(location);
        }

        // PUT: api/Locations/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Locations/5
        public void Delete(int id)
        {
        }
    }
}
