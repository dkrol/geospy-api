﻿using GeoSpy.API.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace GeoSpy.API.Models
{
    public class UserRepository : BaseRepository<User>, IRepository<User>
    {
        public UserRepository()
        {
            base.path = @"~/App_Data/user.json";
        }
        public override User Create()
        {
            User product = new User();
            return product;
        }

        public override List<User> Retrive()
        {
            var filePath = HostingEnvironment.MapPath(path);
            var json = File.ReadAllText(filePath);
            var users = JsonConvert.DeserializeObject<List<User>>(json);
            return users;
        }

        public override User Retrive(int id)
        {
            var users = this.Retrive();
            var user = users.Where(u => u.Id == id).FirstOrDefault();
            return user;
        }

        public override User Save(User user)
        {
            var users = this.Retrive();
            var maxId = users.Max(u => u.Id);
            user.Id = maxId + 1;
            users.Add(user);
            WriteData(users);
            return user;
        }

        public override User Save(int id, User user)
        {
            var users = this.Retrive();
            var itemIndex = users.FindIndex(u => u.Id == id);
            if (itemIndex > 0)
            {
                users[itemIndex] = user;
            }
            else
            {
                return null;
            }
            WriteData(users);
            return user;
        }

        protected override bool WriteData(List<User> users)
        {
            var filePath = HostingEnvironment.MapPath(path);
            var json = JsonConvert.SerializeObject(users, Formatting.Indented);
            File.WriteAllText(filePath, json);
            return true;
        } 
    }
}