﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DAL.MongoDB
{
    public static class MongoConst
    {
        public const string GEOSPYDB = "geospydb";
        public const string GS_LOCATION = "gs_location";
        public const string GS_DEVICE = "gs_device";
    }
}