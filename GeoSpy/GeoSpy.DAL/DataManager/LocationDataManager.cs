﻿using GeoSpy.DAL.MongoDB;
using GeoSpy.DTO.DTO;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DAL.DataManager
{
    public class LocationDataManager
    {
        private readonly Mongo mongo;

        public LocationDataManager()
        {
            mongo = new Mongo(MongoConst.GS_LOCATION);
        }

        public bool AddLocation(Location location)
        {
            bool result = false;
            if (location != null)
            {
                IMongoCollection<Location> coll = mongo.Db.GetCollection<Location>(mongo.CollectionName);
                coll.InsertOne(location);
                result = true;      
            }
            return result;
        }

        public bool AddLocations(List<Location> locations)
        {
            bool result = false;
            if (locations != null)
            {
                IMongoCollection<Location> coll = mongo.Db.GetCollection<Location>(mongo.CollectionName);
                coll.InsertMany(locations);
                result = true;
            }
            return result;
        }

        public Location GetLocationByDeviceId(string deviceId)
        {
            Location currentLocation = null;
            IMongoCollection<Location> coll = mongo.Db.GetCollection<Location>(mongo.CollectionName);
            currentLocation = coll.AsQueryable<Location>().Where(loc => loc.DeviceId == deviceId).OrderByDescending(loc => loc.Timestamp).FirstOrDefault();
            return currentLocation;
        }

        public IQueryable<Location> GetLocationsByDevice(Device device)
        {
            IMongoCollection<Location> coll = mongo.Db.GetCollection<Location>(mongo.CollectionName);
            IQueryable<Location> result = coll.AsQueryable<Location>().Where(loc => loc.DeviceId == device.Id);
            return result;
        }

        public IQueryable<Location> GetLocationsByDeviceAndDatetimeRange(string deviceId, DatetimeRange datetimeRange)
        {
            List<Location> result = null;
            IMongoCollection<Location> coll = mongo.Db.GetCollection<Location>(mongo.CollectionName);
            List<Location> locations = coll.AsQueryable<Location>().Where(loc => loc.DeviceId == deviceId).ToList();
            if (locations != null && locations.Count > 0)
            {
                result = new List<Location>();
                foreach (var loc in locations)
                {
                    DateTime locTimestamp = DateTime.Parse(loc.Timestamp);
                    if (locTimestamp >= datetimeRange.FromDateTime && locTimestamp <= datetimeRange.ToDateTime)
                    {
                        result.Add(loc);
                    }
                }
            }
            return result.AsQueryable();
        }

        public IEnumerable<Location> GetLocations()
        {
            IMongoCollection<Location> coll = mongo.Db.GetCollection<Location>(mongo.CollectionName);
            return coll.AsQueryable<Location>();
        }
    }
}