﻿using GeoSpy.DAL.MongoDB;
using GeoSpy.DTO.DTO;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DAL.DataManager
{
    public class DeviceDataManager
    {
        private readonly Mongo mongo;
        public DeviceDataManager()
        {
            mongo = new Mongo(MongoConst.GS_DEVICE);
        }

        public Device AddDevice(Device device)
        {
            Device result = null;
            if (device != null)
            {
                IMongoCollection<Device> coll = mongo.Db.GetCollection<Device>(mongo.CollectionName);
                Device deviceDb = coll.Find<Device>(dev => dev.UserName == device.UserName && dev.Imei == device.Imei).FirstOrDefault();
                if (deviceDb != null)
                {
                    device.Id = deviceDb.Id;
                    if (deviceDb.Name != device.Name)
                    {
                        var filter = Builders<Device>.Filter.Eq(dev => dev.Id, deviceDb.Id);
                        var mongoResult = coll.ReplaceOne(filter, device);
                    }
                }
                else
                {
                    coll.InsertOne(device);
                }
                result = device;
            }
            return result;
        }

        public IQueryable GetDeviceByUserName(string userName)
        {
            IMongoCollection<Device> coll = mongo.Db.GetCollection<Device>(mongo.CollectionName);
            IQueryable<Device> result = coll.AsQueryable<Device>().Where(dev => dev.UserName == userName);
            return result;
        }

        public Device GetDevice(string imei)
        {
            IMongoCollection<Device> coll = mongo.Db.GetCollection<Device>(mongo.CollectionName);
            Device result = coll.Find<Device>(dev => dev.Imei == imei).FirstOrDefault();
            return result;
        }

        public void UpdateDevice(string id, Device device)
        {
            // TODO: Update device

        }

        public List<Device> GetDevices()
        {
            IMongoCollection<Device> coll = mongo.Db.GetCollection<Device>(mongo.CollectionName);
            return coll.AsQueryable<Device>().ToList();
        }
    }
}