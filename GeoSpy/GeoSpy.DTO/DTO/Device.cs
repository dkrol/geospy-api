﻿using GeoSpy.DTO.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DTO.DTO
{
    public class Device
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Imei { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
    }
}