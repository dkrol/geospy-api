﻿using GeoSpy.API.Base;
using GeoSpy.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace GeoSpy.API.Controllers
{
    [EnableCorsAttribute("http://localhost:3000", "*", "*")]
    public class UsersController : ApiController
    {
        private const string NULL_PARAMS = "Object cannot be null";
        //private BaseRepository<User> repository = new UserRepository();
        private UserRepository repository = new UserRepository();
        // GET: api/User
        [ResponseType(typeof(User))]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(repository.Retrive());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }

        // GET: api/User/5
        [ResponseType(typeof(User))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                User user;
                if (id > 0)
                {
                    user = repository.Retrive(id);
                    if (user == null)
                    {
                        return NotFound();
                    }
                }
                else
                {
                    user = repository.Create();
                }
                return Ok(user);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }

        // POST: api/User
        [ResponseType(typeof(User))]
        public IHttpActionResult Post([FromBody]User user)
        {
            try
            {
                if (user == null)
                {
                    return BadRequest(NULL_PARAMS);
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var newUser = repository.Save(user);
                if (newUser == null)
                {
                    return Conflict();
                }

                return Created<User>(Request.RequestUri + newUser.Id.ToString(), newUser);
            } catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }

        // PUT: api/User/5
        public IHttpActionResult Put(int id, [FromBody]User user)
        {
            try
            {
                if (user == null)
                {
                    return BadRequest(NULL_PARAMS);
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var updatetUser = repository.Save(id, user);

                if (updatetUser == null)
                {
                    return NotFound();
                }

                return Ok(updatetUser);
            } catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
