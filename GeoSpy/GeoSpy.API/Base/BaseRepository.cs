﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.API.Base
{
    public abstract class BaseRepository<T> : IRepository<T>
    {
        protected string path { get; set; }

        public abstract T Create();
        public abstract List<T> Retrive();

        public abstract T Retrive(int id);

        public abstract T Save(T product);

        public abstract T Save(int id, T obj);

        protected abstract bool WriteData(List<T> obj);
    }
}