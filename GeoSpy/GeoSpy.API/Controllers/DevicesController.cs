﻿using GeoSpy.DAL.DataManager;
using GeoSpy.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GeoSpy.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Devices")]
    [EnableCorsAttribute("http://localhost:3000", "*", "*")]
    public class DevicesController : ApiController
    {
        private DeviceDataManager deviceDataManager = new DeviceDataManager();
        // GET: api/Devices
        public IEnumerable<Device> Get()
        {
            return deviceDataManager.GetDevices();
        }

        //// GET: api/Devices/5
        //public Device Get(string imei)
        //{
        //    return deviceDataManager.GetDevice(imei);
        //}

        // GET: api/Devices/User/
        [Route("User")]
        public IHttpActionResult Post([FromBody]UserDb user)
        {
            try
            {
                return Ok(deviceDataManager.GetDeviceByUserName(user.UserName));
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Devices
        public IHttpActionResult Post([FromBody]Device device)
        {
            try
            {
                Device addedDevice = deviceDataManager.AddDevice(device);
                return Ok(addedDevice);
            } 
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Devices/5
        public void Put(string id,[FromBody]Device device)
        {
            deviceDataManager.UpdateDevice(id, device);
        }

        // DELETE: api/Devices/5
        public void Delete(int id)
        {
        }
    }
}
