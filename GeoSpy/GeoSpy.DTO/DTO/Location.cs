﻿using GeoSpy.DTO.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DTO.DTO
{
    public class Location
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double Speed { get; set; }
        public string Timestamp { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string DeviceId { get; set; }
    }
}