﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoSpy.API.Base
{
    interface IRepository<T>
    {
        T Create();
        List<T> Retrive();
        T Retrive(int id);
        T Save(T obj);
        T Save(int id, T obj);
    }
}
