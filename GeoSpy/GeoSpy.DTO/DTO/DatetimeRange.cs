﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DTO.DTO
{
    public class DatetimeRange
    {
        public string From { get; set; }
        public string To { get; set; }

        public DateTime FromDateTime
        {
            get
            {
                return DateTime.Parse(From);
            }
        }
        public DateTime ToDateTime
        {
            get
            {
                return DateTime.Parse(To);
            }
        }
    }
}