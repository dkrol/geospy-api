﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoSpy.DTO.DTO
{
    public class UserDb
    {
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}