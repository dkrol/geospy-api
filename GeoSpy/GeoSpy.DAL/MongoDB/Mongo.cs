﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GeoSpy.DTO.Interfaces;

namespace GeoSpy.DAL.MongoDB
{
    public class Mongo
    {
        public MongoClient Client
        {
            get
            {
                return client;
            }
        }

        public IMongoDatabase Db
        {
            get
            {
                return db;
            }
        }
        public string CollectionName
        {
            get
            {
                return collectionName;
            }
        }
        private MongoClient client;
        private IMongoDatabase db;
        private string collectionName;
        public Mongo(string collectionName)
        {
            this.client = new MongoClient(); // Default connection to localhost:27017
            this.db = client.GetDatabase(MongoConst.GEOSPYDB);
            this.collectionName = collectionName;
        }
    }
}